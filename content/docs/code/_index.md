---
bookCollapseSection: true
weight: 20
bookToC: false
Title: Code
---

I enjoy writing code. I've collected some of my favorite code fragments and tricks, mostly in python, here.