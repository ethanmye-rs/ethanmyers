---
bookCollapseSection: False
weight: 10
title: Short Tricks
---

# Short Tricks

## Writing big numbers nicely
Python will interpret `100_000_000_000` as `100000000000`. I think the former is much easier to read.

	>>> 100_000_000_000 is 100000000000 & 100_000_000_000 == 100000000000
	True


## Quickly commenting a block of code
Python has no block comments. Instead, use `CTRL` + `\` on a highlighted block of text to comment it out

## Print convenience concatenation
Python will concatenate things for you. Sometime very annoying, sometimes helpful:

	>>> print("Ethan " "Myers")
	Ethan Myers