---
Title: Carriage Lock
bookToC: false

---

# Designing a Carriage Lock

{{< youtube W3iqkKz2sO0 >}}
*Short demo of the clamping effectiveness*

A simple part, but the first real upgrade I've made for my lathe. The top is 30.1mm x 25.50mm wide, while the base is 30.1mm x 39.1mm wide. Total thickness is 12.75mm, and the shoulders are 8mm tall. The only critical dimension here is the top 25.50 measurement -- Ideally, it's tight enough to not wobble as it moves through the bed, but not so tight as to bind. I found my bed to very loosely (and coarsely!) machined. It's something I hope to clean up in the future, but it not critical.

![Finished][1]
*Finished part*

[1]: /img/carriageclamp/finished.jpg



