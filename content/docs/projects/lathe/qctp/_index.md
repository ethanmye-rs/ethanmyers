---
Title: Quick Change Tool Post
bookToC: false

---

# Building a Quick Change Tool Post
# *In Progress*

*I'm still updating this. Each header represents a todo*

![Finished][1]
*CAD Model of QCTP*

I'm building a QCTP for my lathe. Officially, this is a learning exercise in flexture design, but it's also an opportunity for my to avoid spending $100 for some more coarsely finished eBay machinery.

![Stock][2]
*Stock*

I chose to use steel stock here. The block is of unknown origin, but pretty soft. It's 50.00 x 50.00 x 50.00 ± 0.05mm across all faces.

## Why Flexures 

## Flexure Calculations

## Deflection Results

## Tool Holder Fabrication

## In use







[1]: /img/qctp/body.PNG
[2]: /img/qctp/stock.jpg


