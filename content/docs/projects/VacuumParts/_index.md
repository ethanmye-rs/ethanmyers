---
bookflatSection: false
weight: 10
title: Moth Eater
---
# Besieged by Moths.

*I’m still updating this*

For two weeks every summer, my house is besieged. Not by men, not by mice, but by moths. Thousands of them arrive over the weeks; hundreds die inside daily. They're called Miller moths colloquially, but they're actually [army cutworms](https://en.wikipedia.org/wiki/Army_cutworm), a common agricultural pest.

They fly in through screen doors, chimneys and vents, and lodge themselves firmly in every window and light fixture available. Because Nature optimized for calorie count (they're 70+% fat by weight, making them a bear favorite) and not intelligence, they promptly throw themselves at the window until they die. 

This is annoying, both to clean and to listen to. So, I'm building a small trap to keep them there. While I'm a little late for moth season, I hope to have the "in progress" tag off this before next year.

## Vacuum Solution
 *a small vacuum ripped from a robotic vacuum cleaner, a blue LED, and a funnel*

Admittedly the fanciest solution, and probably the worst. 

## Conventional Fly Zapper
 *a high voltage module, a wire grid, and a blue LED*

## U Tube
 *a u shaped tube, blue LED, and zapper*

