---
bookCollapseSection: true
weight: 20
Title: Server
---

# Server


## Why?

A mixture of curiosity, annoyance, and frustration -- the negligent stepmother of invention. I have a large media collection of music, books, photos, etc. I usually want to be able to share this with my family and friends, but every vendor seems to want to make this as aggravating as possible. What's more, it's immensely irritating having a file on my PC but not my phone, and vice versa. Running my own server provides a lesson in Linux administration, as well as removes a large pain point in my life. I hope it also provides some valuable insurance in the form of a backup for pictures and other media.

## What?

A Gen 10 HP Enterprise Microserver. It's not as cool as the Gen8, as it has a socketed CPU. That being said, this is basically a NAS plus some services, so no real power is needed here.