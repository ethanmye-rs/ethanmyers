---
bookCollapseSection: true
weight: 10
bookToC: false
Title: Projects
---

# Projects: Turning $100 into $10

Working on projects is a labor of love. It better be, because they are, in absolute terms, horrendously unproductive and hideously expensive. Painful bean counting aside, projects are pretty much the only way to confidently build skill in technical disciplines. This is an evolving document for the meta of projects, check the sidebar for actual projects.

## The futility of out-competing mass production

Most of the time, I start out on a project frustrated with commercial options. I've scoured eBay, Craigslist, and Alibaba without a single cheap, good, and quick option -- surprising, I know. Next, a Google patent search, distilling a complex idea into a handful of fabricobble-able subsystems I could build in a few weeks. Then, back to eBay et al. for cheap components of woefully inadequate capability and generally dubious origin. A few weeks later, and I've turned a $100 into $10 of functionality.


{{< columns >}}

I have a terrible habit of misjudging the difficult part of projects, particuraly physical ones. Programming is much easier to level the playing field, so many obscenely powerful tools are just a few clicks away. You can be up in running with the most powerful software tools on the planet in a few hours, even on a POS computer. 

<--->


Physical creation takes a bit more. A VMC is 10k pounds, a lathe another few thousand, a press brake, a surface grinder, a manual mill... the list goes on. A well equipped, one man shop may have 100,000 lbs of machinery in it, especially when bigger tools (waterjet, inspection equipment, laser cutter) make it into the shop. Even at scrap metal prices of $0.50 a pound, physical creation takes orders of magnitude more cash to run with the pros.


{{< /columns >}}

There's also the tyranny of marginal cost to consider. The first prototype may take $100, but subsequent copies take far, far less, usually several orders of magnitude. As such, just building one ends up being an expensive proposition.



## Skill Building

However, this is all not a waste. I've been fortunate to learn so much in school, from excellent teachers who, for the most part, care. That being said, I've learned 80+% of what I know on my own. Whether is be PCB design, machining, programming, or cooking, I found a great common base provided by school (mostly math and English) and enough free time to put it to use. So many people malign school for useless content, but fail to see just how pervasive it is; communication is so much easier when everyone has a basic competency. 

Building a project forces me to understand every little bit of a design. It's easy to gloss over detail in a book; I frequently find myself believing with all my heart a concept, then being forced to prove it, and becoming convinced I've been tricked. Trick or not, there is a massive difference in understanding from those who do and those who see. I believe with all my heart that I do not understand until I do.  








