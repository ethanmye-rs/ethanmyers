---
bookCollapseSection: true
weight: 20
bookToC: false
Title: Essays
---

I enjoy writing. Rather than stuffing essays into my hard drive for no one to see, I've decided to dump them here. They're my personal opinions, and will evolve. If you need a static copy, check the Gitlab this is hosted in. You'll be able to see all the edits I've made.

The language is flowery, because I find it enjoyable to read. I'm not pressed for bandwidth; I'm going to give main idea a bit of breathing room (and a touch of repetition).