---
type: docs
bookToc: false

---

# Hello

My name is Ethan Myers.

I'm currently a student at a large public research university in the US. This is a small site I put together to showcase my projects, keep a public reading list, and host a few essays I've written. It's more than a public display though. The site also serves to keep me accountable to my projects, share progress, and let me solicit feedback on my writing.

My résumé is viewable [here](/pdf/CV_US.pdf).

I can be reached via <ethan@ethanmye.rs>

The site is built with [Hugo](https://gohugo.io/). It's hosted by [Netlify](https://netlify.com/). The theme is the excellent [book](https://github.com/alex-shpak/hugo-book) by Alex Shpak.

