---
menu:
  after:
    name: books
    weight: 3
title: Books
---

# Books I like

I love to read. This is a list of novels I own and have read or am otherwise interested in. Some have short reviews/thoughts. I've also listed a few of my favorite technical books. 

I'm always interested in loaning or trading.


{{< columns >}}
## Novels

* **East Of Eden** *John Steinbeck*
* **A People's History of The United States** *Howard Zinn*
* **Ficciones**  *Jorge Borges*
* **Cannery Row** *John Steinbeck*
* **Do Androids Dream of Electric Sheep** - *Phillip K Dick*
* **Shoe Dog** *Phil Knight*
* **Fahrenheit 451** *Ray Bradbury*
* **We** *Yevgeny Zamyatin*
* **Othello** *William Shakespeare*
* **The Count of Monte Cristo** *Alexander Dumas*
* **Catch-22** *Joseph Heller*
* **Liar's Poker** *Michael Lewis*
* **Brave New World** *Aldous Huxley*
* **Amusing Ourselves to Death** *Neil Postman*
* **The Grapes of Wrath** *John Steinbeck*
* **Flash Boys** *Michael Lewis*
* **Flash Boys: Not So Fast** *Peter Kovac*
* **The Merchant of Venice** *William Shakespeare*
* **The Little Prince** *Antoine de Saint-Exupéry*
* **Dreamland** *Sam Quinones*
* **The Importance of being Earnest** *Oscar Wilde*
* **1776** *David McCullough*
* **The True Story of Ah-Q** *Lu Xun*
* **Animal Farm** *George Orwell*
* **1984** *George Orwell*
* **Bless Me, Ultima** *Rudolfo Anaya*
* **The God of Small Things** *Arundhati Roy*
* **The Alchemist** *Paulo Coelho*
* **The Metamorphosis** *Franz Kafka*
* **Things Fall Apart**  *Chinua Achebe*
* **Midnights Children**  *Salman Rushdie*
* **Lolita** *Vladimir Nabokov*
* **The Adventures of Huckleberry Finn** *Mark Twain*
* **Slaughterhouse Five** *Kurt Vonnegut*
* **Infinite Jest**  *David Foster Wallace*
* **How to Win Friends and Influence People** *Dale Carnegie*
* **Homage to Catalonia** *George Orwell*
* **The Prince** *Niccolò Machiavelli*
* **Lord of the Flies** *William Golding*
* **Sapiens** *Yuval Noah Harari*
* **Beloved** *Toni Morison*
* **The Giving Tree** *Shel Silverstein*
* **Ready Player One** *Ernest Cline*
* **The Alchemy of Air** *Thomas Hager*
* **The Sympathizer** *Viet Thanh Nguyen*
* **Rules for Radicals** *Saul Alinsky*
* **GEB** *Michael Hofstadter*
* **The Power Broker** *Robert Caro*
* **Guns Germs and Steel** *Jared Diamond*
* **Dark Pools** *Scott Patterson*
* **[Fooled By Randomness](fooledbyrandomness)** *Nassim Taleb*
* **[Concrete Planet](concreteplanet)** *Robert Courland*
* **The Code Book** *Simon Singh*

<--->

## Technical Books
* **Introduction to Electrodynamics** *Griffiths*
* **The Art of Electronics** *Horowitz, Hill*
* **Fluent Python** *Ramalho*
* **Mathematical Methods in the Physical Sciences** *Boas*
* **Procedures in Experimental Physics** *Strong*
